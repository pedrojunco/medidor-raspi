<?php 

class Modelo_general extends CI_Model {
    
    public function login($usuario, $pass){
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where(array('usuario'=>$usuario,'password'=>$pass, 'estatus'=>1));
        $consulta = $this->db->get();
        
        if($consulta->num_rows() > 0){
            return $consulta->row_array();
        }else{
            return false;
        }
    }
    
    public function consultar_lecturas(){
        $this->db->select('*');
        $this->db->from('registros');
        $this->db->order_by('id_registro',"DESC");
        //$this->db->limit(1);
        $consulta = $this->db->get();
        
        return $consulta->result_array();
    }
    
    public function registro_temperatura($servidor, $farenheit, $celcius){
        $data = array(
            "servidor"=>$servidor,
            "celcius"=>$celcius,
            "farenheit"=>$farenheit
            );
        $consulta = $this->db->insert('registros',$data);
        
        if($consulta){
            return "1";
        }else{
            return "0";
        }
    }
    
}


?>