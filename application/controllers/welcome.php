<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	function __construct() {
       parent::__construct();
       header('Access-Control-Allow-Origin: *');
       $this->load->model("modelo_general","",TRUE);
   }
	
	public function index(){
		$this->load->view('welcome_message');
	}
	
	public function login(){
		$data_post = file_get_contents("php://input");
        $data = json_decode($data_post);
        
		$usuario = $data->usuario;
		$password = $data->password;
		
		$respuesta = $this->modelo_general->login($usuario, $password);
			
		echo json_encode($respuesta);
	}
	
	public function consultar_lecturas(){
		$respuesta = $this->modelo_general->consultar_lecturas();
		
		echo json_encode($respuesta);
	}
	
	// https://raspi-ionic-pedrojunco.c9users.io/welcome/registro_temperatura?servidor=C418&farenheit=72.56&celcius=38.15
	public function registro_temperatura(){ 
		$servidor = $this->input->get('servidor');
		$farenheit = $this->input->get("farenheit");
        $celcius = $this->input->get("celcius");
		$fecha = date('Y-m-d');
		$hora = date('H:i:s');
		
		
		if(floatval($celcius) > 35){
			//$url = 	"https://pruebapush-59a81.firebaseio.com/".$servidor.".json";
			$url = 	"https://medidor-f720a.firebaseio.com/".$servidor.".json";
			$data = '{ 
				"celcius" : "'.$celcius.'", 
				"farenheit" : "'.$farenheit.'", 
				"fecha" : "'.$fecha.'", 
				"hora" : "'.$hora.'" 
			}';
			
			$ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_POST, 1); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // remove body 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/plain")); // remove body 
            $response = curl_exec($ch); 
            
            if(curl_errno($ch)){
            	echo 'Error'.curl_errno($ch);
            }else{
            	echo 'se inserto';
            }
            curl_close($ch); 
		}	
		
		
		$respuesta = $this->modelo_general->registro_temperatura($servidor, $farenheit, $celcius);
		
        echo $respuesta;
	}
	
	function sendMessage(){
        // 6fd56e6d-0d2b-4485-825b-5be31d9be2ef
		$content = array(
			"en" => "Temperatura registrada"
			);

		
		$fields = array(
			'app_id' => "53a420ec-60cc-4f8a-996e-caa206aed2dd",
			'included_segments' => array('All'),
    		'data' => array("foo" => "bar"),
			'contents' => $content
		);
    	$this->initOneSignal($fields);
	}

	function initOneSignal($fields){
	    $fields = json_encode($fields);
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic ZjQwYzhhNTAtNTA2My00ODMzLThlNTAtNGE4OGM2MjhiMjU2'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
	}
	
}

//$usuario = $this->input->post('usuario');
//$pass = $this->input->post('password');
		
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */